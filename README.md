# :bulb: Lagalt - API for The Great Project Manager :bulb:

### By [Tanja Vinogradova](https://gitlab.com/tavinogradova92), [Andreas Kjelstrup](https://gitlab.com/AndreasKjelstrup) and [Rune Hou Thode](https://gitlab.com/rhout)

<br>

## Installation :computer:

Clone the repo with
`git clone https://gitlab.com/rhout/lagalt-back.git`

Open the project with Eclipse, Netbeans, IntelliJ or your favourite IDE.

## Requirements :white_check_mark:

This application is created using JDK15 and IntelliJ and has not been tested for any other versions. The following steps might differ if you use a different IDE.

The data is stored in a Postgres :elephant: database, so to create a local database you can work with, download and
install [PostgreSQL](https://www.postgresql.org/). Create a database in pgAdmin called lagalt (use default port 5432) or update the _spring.datasource.url_ in [application.properties](./src/main/resources/application.properties) if you want your own database name.

**Note:** _application.properties contains default credentials. Remember to change the username and password to whatever you chose. **You should never store your password in plaintext.**_

Once the database is created, run it to create the tables. Now you are ready to roll! :tada: Check out the API [documentation](https://lagalt-app.herokuapp.com/api/v1/swagger-ui.html) to see what you can do. You can also use the great [frontend](https://gitlab.com/rhout/lagalt-front) made for this API.

It is only possible to create projects and users, so if you want some default data to play around with, run the [master script](./src/main/resources/mock%20data/1_master_script.sql) to seed the database.

## Amazon S3 :large_orange_diamond:

In order to make the image uploads work, you should create/add an existing [S3](https://aws.amazon.com/s3/) bucket. When you have done so, add the following information in bucket information in [application.properties](./src/main/resources/application.properties):

```
amazon.s3.bucket-name=/* AMAZON_BUCKET_NAME */
amazon.s3.endpoint=/* AMAZON_BUCKET_ENDPOINT */
amazon.s3.access-key=/* AMAZON_ACCESS_KEY */
amazon.s3.secret-key=/* AMAZON_SECRET_KEY */

```
