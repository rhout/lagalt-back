package dk.experis.lagalt.repositories;

import dk.experis.lagalt.models.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;


    @Test
    void getAllIndustries() {
        entityManager.persist(new User("Rune", "I like to play music", "myemail@mail.xom"));
        entityManager.persist(new User("Rune", "I like to play music", "myemail@mail.xom"));
        entityManager.persist(new User("Rune", "I like to play music", "myemail@mail.xom"));
        List<User> users = userRepository.findAll();
        assertEquals("Rune", users.get(0).getName());
        assertEquals(3, users.size());
    }
}