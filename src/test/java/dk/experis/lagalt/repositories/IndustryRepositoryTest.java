package dk.experis.lagalt.repositories;

import dk.experis.lagalt.models.Industry;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class IndustryRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private IndustryRepository industryRepository;


    @Test
    void getAllIndustries() {
        entityManager.persist(new Industry("Game design", "https://gitlab.com/rhout/lagalt-images/-/raw/master/industries/src_assets_images_gamedesign.png"));
        entityManager.persist(new Industry("Film making", "images.google.com"));
        entityManager.persist(new Industry("Web development", "images.google.com"));
        entityManager.persist(new Industry("Music making", "images.google.com"));
        List<Industry> industries = industryRepository.findAll();
        assertEquals("Game design", industries.get(0).getName());
        assertEquals("https://gitlab.com/rhout/lagalt-images/-/raw/master/industries/src_assets_images_gamedesign.png", industries.get(0).getImage());
        assertEquals(4, industries.size());
    }
}