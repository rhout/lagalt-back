-- USERS --

INSERT INTO users(date_created, deleted, description, email, hidden, name, image, portfolio)
VALUES(date(localtimestamp), false, 'This is a description of a user', 'mail@mail.com', false, 'Bob', 'https://gitlab.com/rhout/lagalt-images/-/raw/master/profiles/male-avatar2.jpg', 'https://github.com/');

INSERT INTO users(date_created, deleted, description, email, hidden, name, image, portfolio)
VALUES(date(localtimestamp), false, 'Some web developer', 'somePeterEmail@Peter.com', true, 'Peter', 'https://gitlab.com/rhout/lagalt-images/-/raw/master/profiles/female-avatar.png', '');

INSERT INTO users(date_created, deleted, description, email, hidden, name, image, portfolio)
VALUES(date(localtimestamp), false, 'Some nerd', 'andreas@andreasmail.com', false, 'Andreas', 'https://gitlab.com/rhout/lagalt-images/-/raw/master/profiles/male-avatar1.png', 'https://gitlab.com/AndreasKjelstrup');

-- TAGS --

INSERT INTO tags (name)
VALUES ('Platformer');

INSERT INTO tags (name)
VALUES ('Drinks');

INSERT INTO tags (name)
VALUES ('Music');


-- SKILLS --
INSERT INTO skills(skill) VALUES(0);
INSERT INTO skills(skill) VALUES(1);
INSERT INTO skills(skill) VALUES(2);
INSERT INTO skills(skill) VALUES(3);
INSERT INTO skills(skill) VALUES(4);
INSERT INTO skills(skill) VALUES(5);


-- INDUSTRIES --
INSERT INTO industries (name, image)
VALUES ('Game development', 'https://gitlab.com/rhout/lagalt-images/-/raw/master/industries/src_assets_images_gamedesign.png');

INSERT INTO industries (name, image)
VALUES ('Web development', 'https://gitlab.com/rhout/lagalt-images/-/raw/master/industries/src_assets_images_webdevelopment.png');

INSERT INTO industries (name, image)
VALUES ('Music', 'https://gitlab.com/rhout/lagalt-images/-/raw/master/industries/src_assets_images_music.png');

INSERT INTO industries (name, image)
VALUES ('Film making', 'https://gitlab.com/rhout/lagalt-images/-/raw/master/industries/src_assets_images_filmmaking.png');





-- USER_SKILL --
INSERT INTO user_skill(user_id, skill_id) VALUES(1,1);

INSERT INTO user_skill(user_id, skill_id) VALUES(1,2);

INSERT INTO user_skill(user_id, skill_id) VALUES(1,3);

INSERT INTO user_skill(user_id, skill_id) VALUES(2,1);

INSERT INTO user_skill(user_id, skill_id) VALUES(2,2);

INSERT INTO user_skill(user_id, skill_id) VALUES(2,3);

INSERT INTO user_skill(user_id, skill_id) VALUES(3,1);

INSERT INTO user_skill(user_id, skill_id) VALUES(3,4);



-- PROJECT --
INSERT INTO projects (date_created, deleted, description, name, progress, industry_id, project_image)
VALUES ('2019-11-30', false, 'We want to make a movie about how fast peacocks can run.', 'The running peacock.', 3, 4, 'https://lagalt.s3.eu-north-1.amazonaws.com/1608197455042-road-1072823_640.jpg');

INSERT INTO projects (date_created, deleted, description, name, progress, industry_id, project_image)
VALUES ('2018-11-30', false, 'Someone told me that super heroes are popular.', 'Avengers: End Game', 3, 4, 'https://lagalt.s3.eu-north-1.amazonaws.com/1608197460863-forest-931706_640.jpg');

INSERT INTO projects (date_created, deleted, description, name, progress, industry_id, project_image)
VALUES ('2020-01-17', false, 'A game where you can climb buildings and fight people. Never seen before.', 'Aladdins Need MXII', 3, 1, 'https://lagalt.s3.eu-north-1.amazonaws.com/1608197465009-flowers-324175_640.jpg');

INSERT INTO projects (date_created, deleted, description, name, progress, industry_id, project_image)
VALUES ('2020-11-30', false, 'A project for showing off our awesome full stack skills.', 'Lagalt', 1, 2, 'https://lagalt.s3.eu-north-1.amazonaws.com/1608197469418-dandelion-445228_640.jpg');

INSERT INTO projects (date_created, deleted, description, name, progress, industry_id, project_image)
VALUES ('2020-09-28', true, 'TBA', 'No name project.', 0, 3, 'https://lagalt.s3.eu-north-1.amazonaws.com/1608197473323-elephant-1822636_640.jpg');

INSERT INTO projects (date_created, deleted, description, name, progress, industry_id, project_image)
VALUES ('2018-04-12', false, 'Hey all! Let us build a time machine that can be used on the web!!', 'The web time machine', 2, 2, 'https://lagalt.s3.eu-north-1.amazonaws.com/1608197477080-railroad-163518_640.jpg');




-- PROJECT_TAG --
INSERT INTO project_tag (project_id, tag_id)
VALUES (1, 1);

INSERT INTO project_tag (project_id, tag_id)
VALUES (1, 2);

INSERT INTO project_tag (project_id, tag_id)
VALUES (1, 3);

INSERT INTO project_tag (project_id, tag_id)
VALUES (2, 1);

INSERT INTO project_tag (project_id, tag_id)
VALUES (2, 3);

INSERT INTO project_tag (project_id, tag_id)
VALUES (3, 2);

INSERT INTO project_tag (project_id, tag_id)
VALUES (4, 3);



-- USER_PROJECT --
INSERT INTO user_project (is_active, is_owner, project_id, user_id)
VALUES (true, true, 1, 1);

INSERT INTO user_project (is_active, is_owner, project_id, user_id)
VALUES (true, false, 1, 2);

INSERT INTO user_project (is_active, is_owner, project_id, user_id)
VALUES (false, false, 1, 3);

INSERT INTO user_project (is_active, is_owner, project_id, user_id)
VALUES (true, true, 2, 2);

INSERT INTO user_project (is_active, is_owner, project_id, user_id)
VALUES (true, true, 2, 3);

INSERT INTO user_project (is_active, is_owner, project_id, user_id)
VALUES (false, false, 2, 1);

INSERT INTO user_project (is_active, is_owner, project_id, user_id)
VALUES (true, true, 3, 3);

INSERT INTO user_project (is_active, is_owner, project_id, user_id)
VALUES (false, true, 4, 3);

INSERT INTO user_project (is_active, is_owner, project_id, user_id)
VALUES (true, true, 5, 2);

INSERT INTO user_project (is_active, is_owner, project_id, user_id)
VALUES (true, false, 2, 5);



-- PROJECT_SKILL --

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (false, 1, 5);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (true, 1, 6);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (false, 2, 5);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (true, 2, 6);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (false, 3, 1);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (true, 3, 2);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (false, 4, 1);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (true, 4, 3);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (false, 4, 5);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (false, 5, 3);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (false, 5, 4);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (false, 5, 5);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (false, 5, 6);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (true, 6, 1);

INSERT INTO project_skill (is_needed, project_id, skill_id)
VALUES (false, 6, 5);





-- MESSAGES --
INSERT INTO messages(date_created, deleted, message, title, user_project_id)
VALUES(date(localtimestamp), false, 'Welcome everyone, glad to have you onboard this project!', 'Welcome!', 1);

INSERT INTO messages(date_created, deleted, message, title, user_project_id)
VALUES(date(localtimestamp), false, 'The project has been finished, congratulations', 'FINISHED', 2);

INSERT INTO messages(date_created, deleted, message, title, user_project_id)
VALUES(date(localtimestamp), false, 'Test message', 'Test', 3);


-- CHATS --
INSERT INTO chats(chat_message, date_created, deleted, user_project_id)
VALUES('Hello Peter, this is John', date(localtimestamp), false, 1);

INSERT INTO chats(chat_message, date_created, deleted, user_project_id)
VALUES('Hello John, welcome to the project!', date(localtimestamp), false, 1);

INSERT INTO chats(chat_message, date_created, deleted, user_project_id)
VALUES('This is just a test chat message', date(localtimestamp), false, 2);

INSERT INTO chats(chat_message, date_created, deleted, user_project_id)
VALUES('Luke, I am your father.', date(localtimestamp), false, 3);



-- APPLICATIONS --
INSERT INTO applications(application, application_approved, application_resolved, date_created, deleted, user_project_id)
VALUES('I would like to join this fantastic project as I can contributed a lot.', true, true, date(localtimestamp), false, 1);

INSERT INTO applications(application, application_approved, application_resolved, date_created, deleted, user_project_id)
VALUES('test application', false, false, date(localtimestamp), false, 2);

INSERT INTO applications(application, application_approved, application_resolved, date_created, deleted, user_project_id)
VALUES('please let me join.', false, true, date(localtimestamp), false, 2);



-- RESOURCES --

INSERT INTO resources (date_created, deleted, resource, resource_type, user_project_id)
VALUES ('2019-08-01', false, 'https://149366112.v2.pressablecdn.com/wp-content/uploads/2020/05/shutterstock_567926224-scaled.jpg', 1, 1);

INSERT INTO resources (date_created, deleted, resource, resource_type, user_project_id)
VALUES ('2020-11-30', false, 'https://pbs.twimg.com/profile_images/378800000515646282/6342efc7e191e5e0e8439e552fcb537a_400x400.jpeg', 1, 2);

INSERT INTO resources (date_created, deleted, resource, resource_type, user_project_id)
VALUES ('2020-05-15', false, 'https://gitlab.com/rhout/lagalt-back', 2, 3);