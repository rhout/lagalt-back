package dk.experis.lagalt.controllers;

import dk.experis.lagalt.models.DTOs.CommonResponse;
import dk.experis.lagalt.models.DTOs.UserDTO;
import dk.experis.lagalt.models.User;
import dk.experis.lagalt.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "User Controller", description = "Get and update a user.")
@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * Gets a specific user based on the users id.
     *
     * @param id
     * @return
     */
    @Operation(summary = "Get user by id.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "A user with the provided id does not exist.")
    @Parameter(name = "id", description = "Id of the user to get.")
    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<CommonResponse> getUserById(@PathVariable long id) {
        User user = userService.getUserById(id);
        HttpStatus status;
        CommonResponse response;
        if (user != null) {
            status = HttpStatus.OK;
            response = new CommonResponse(user);
        } else {
            status = HttpStatus.NO_CONTENT;
            response = new CommonResponse(status);

        }
        return new ResponseEntity<>(response, status);
    }

    /**
     * Updates the changed fields of the user with the specified id.
     *
     * @param userId      Id of the user to update
     * @param updatedUser The user with all the fields to be updated.
     * @return
     */
    @Operation(summary = "Update user")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "404", description = "User with the given id does not exist.")
    @Parameter(name = "id", description = "Id of the user to update.")
    @Parameter(name = "user", description = "The updated usera.")
    @CrossOrigin
    @PatchMapping("/{userId}")
    public ResponseEntity<HttpStatus> updateUser(@PathVariable long userId, @RequestBody UserDTO updatedUser) {
        if (!userService.userExists(userId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        User existingUser = userService.getUserById(userId);
        if (updatedUser.name != null) {
            existingUser.setName(updatedUser.name);
        }

        if (updatedUser.image != null) {
            existingUser.setImage(updatedUser.image);
        }

        if (updatedUser.description != null) {
            existingUser.setDescription(updatedUser.description);
        }

        if (updatedUser.portfolio != null) {
            existingUser.setPortfolio(updatedUser.portfolio);
        }

        if (updatedUser.hidden != null) {
            existingUser.toggleHidden();
        }

        if (updatedUser.skills != null) {
            long existingUsersSkillsSize = existingUser.getSkills().size();
            if (updatedUser.skills.size() > existingUsersSkillsSize) {
                userService.addSkillToUser(existingUser, updatedUser.skills);
            } else {
                userService.deleteSkillFromUser(existingUser, updatedUser.skills);
            }
        }

        userService.updateUser(existingUser);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
