package dk.experis.lagalt.controllers;

import dk.experis.lagalt.models.DTOs.CommonResponse;
import dk.experis.lagalt.models.DTOs.IndustryDTO;
import dk.experis.lagalt.models.Industry;
import dk.experis.lagalt.services.IndustryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;

@Tag(name = "Industry Controller", description = "Get industry information.")
@RestController
@RequestMapping("/industries")
public class IndustryController {
    @Autowired
    private IndustryService industryService;

    /**
     * Gets a list of all industries.
     *
     * @return
     */
    @Operation(summary = "Get all industries")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "There are no industries.")
    @CrossOrigin
    @GetMapping
    public ResponseEntity<CommonResponse> getAllIndustries() {
        HttpStatus status = HttpStatus.OK;
        List<Industry> industries = industryService.getAllIndustries();
        if (industries.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }
        CommonResponse response = new CommonResponse(industries);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Gets a single industry
     *
     * @param id
     * @return
     */
    @Operation(summary = "Get industry", description = "Get an industry with the provided id.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "An industry with the provided id does not exist.")
    @Parameter(name = "id", description = "Id of the industry to get.")
    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<CommonResponse> getIndustry(@PathVariable long id) {
        HttpStatus status = HttpStatus.NO_CONTENT;
        Industry returnIndustry = industryService.getIndustryById(id);
        if (returnIndustry != null) {
            status = HttpStatus.OK;
        }

        CommonResponse response = new CommonResponse(returnIndustry);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Fetches all the industries that appear in a specific project.
     *
     * @param projectId
     * @return
     */
    @Operation(summary = "Get industries by project", description = "Get all industries that a project is currently " +
            "in.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "A project with the provided id does not exist.")
    @Parameter(name = "projectId", description = "Id of the project to get the industries from.")
    @CrossOrigin
    @GetMapping("/project/{projectId}")
    public ResponseEntity<CommonResponse> getIndustriesByProject(@PathVariable long projectId) {
        HttpStatus status = HttpStatus.OK;
        List<IndustryDTO> industries = industryService.getAllByProjectId(projectId);
        if (industries.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }

        CommonResponse response = new CommonResponse(industries);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Fetches four most popular industries based on number of projects.
     */
    @Operation(summary = "Get popular industries", description = "Get the four most popular industries based on " +
            "how many projects are in that industry.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "There are no industries.")
    @CrossOrigin
    @GetMapping("/popular")
    public ResponseEntity<CommonResponse> getPopularIndustries() {
        HttpStatus status = HttpStatus.OK;
        List<Industry> industries = industryService.getAllIndustries();
        if (industries.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }

        industries.sort(Comparator.comparingInt(industry -> ((Industry) industry).getProjects().size()).reversed());

        if (industries.size() >= 4) {
            industries = industries.subList(0, 4);
        }

        CommonResponse response = new CommonResponse(industries);
        return new ResponseEntity<>(response, status);
    }
}


