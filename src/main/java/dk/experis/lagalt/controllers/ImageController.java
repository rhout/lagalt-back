package dk.experis.lagalt.controllers;

import dk.experis.lagalt.models.DTOs.CommonResponse;
import dk.experis.lagalt.services.ImageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Tag(name = "Image Controller", description = "CRUDS for projects")
@RestController
@RequestMapping("/upload-image")
public class ImageController {
    @Autowired
    private ImageService imageService;

    /**
     * Uploading a project image to AWS S3 bucket while creating a new project
     * @param image
     * @return
     */
    @Operation(summary = "Upload image", description = "Uploads an image to the image server.")
    @ApiResponse(responseCode = "201", description = "Image uploaded.")
    @ApiResponse(responseCode = "400", description = "Bad request..")
    @Parameter(name = "image", description = "The image file.")
    @CrossOrigin
    @PostMapping
    public ResponseEntity<CommonResponse> uploadFile(@RequestParam MultipartFile image) {
        HttpStatus status = HttpStatus.CREATED;
        CommonResponse response;
        try {
            String uploadedImageUrl = this.imageService.uploadFile(image);
            response = new CommonResponse((Object) uploadedImageUrl);
        } catch (Exception e) {
            response = new CommonResponse("There was an error uploading the image.");
        }
        return new ResponseEntity<>(response, status);
    }

}
