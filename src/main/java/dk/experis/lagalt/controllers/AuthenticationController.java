package dk.experis.lagalt.controllers;

import dk.experis.lagalt.models.DTOs.CommonResponse;
import dk.experis.lagalt.models.DTOs.NewUserDTO;
import dk.experis.lagalt.models.DTOs.UserCredentialsDTO;
import dk.experis.lagalt.models.DTOs.UserDTO;
import dk.experis.lagalt.models.User;
import dk.experis.lagalt.services.UserService;
import dk.experis.lagalt.utils.JwtUtil;
import dk.experis.lagalt.services.MyUserDetailsService;
//import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Authentication Controller", description = "Handles authentication.")
@RestController
public class AuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private MyUserDetailsService userDetailsService;
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtUtil jwtTokenUtil;

    /**
     * Authenticates a user
     *
     * @param authenticationRequest Email and password credentials.
     * @return
     */
    @Operation(summary = "Authenticate user", description = "Authenticate a user and generate authentication token if" +
            " the credentials are correct.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "401", description = "Incorrect email or password.")
    @Parameter(name = "User credentials", description = "Email and password.", required = true)
    @CrossOrigin
    @PostMapping("/auth")
    public ResponseEntity<CommonResponse> createAuthenticationToken(@RequestBody UserCredentialsDTO authenticationRequest) {
        HttpStatus status = HttpStatus.OK;
        CommonResponse response;
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.email,
                            authenticationRequest.password));

            response = getTokenAndUserFromRequest(authenticationRequest.email);
        } catch (BadCredentialsException | InternalAuthenticationServiceException e) {
            status = HttpStatus.UNAUTHORIZED;
            response = new CommonResponse("Incorrect email or password!");
        }
        return new ResponseEntity<>(response, status);
    }

    /**
     * Checks if a user with the specified email already exists.
     *
     * @param email
     * @return
     */
    @Operation(summary = "Check if user exists", description = "Checks if a user with a given email already exists.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "409", description = "User with that email already exists.")
    @Parameter(name = "Email", description = "The email to check.", required = true)
    @CrossOrigin
    @PostMapping("/registration/check")
    public ResponseEntity<CommonResponse> checkIfUserExists(@RequestBody String email) {
        CommonResponse response;
        HttpStatus status;
        if (!userService.userExistsByEmail(email)) {
            status = HttpStatus.CREATED;
            response = null;
        } else {
            status = HttpStatus.CONFLICT;
            response = new CommonResponse("User with that email already exists");
        }

        return new ResponseEntity<>(response, status);
    }

    /**
     * Registers a new user in the system.
     *
     * @param newUserDTO The user information.
     * @return
     */
    @Operation(summary = "Register user", description = "Regsisters a new user in the system.")
    @ApiResponse(responseCode = "201", description = "User was created.")
    @ApiResponse(responseCode = "400", description = "Bad request.")
    @Parameter(name = "User", description = "The user information to register.", required = true)
    @CrossOrigin
    @PostMapping("/registration")
    public ResponseEntity<CommonResponse> registerUser(@RequestBody NewUserDTO newUserDTO) {
        CommonResponse response;
        HttpStatus status = HttpStatus.CREATED;
        String email = newUserDTO.email;
        String password = passwordEncoder.encode(newUserDTO.password);
        try {
            User newUser = new User(newUserDTO.name, password, newUserDTO.description, newUserDTO.portfolio,
                    newUserDTO.email, newUserDTO.image);
            userService.createUser(newUser);

            response = getTokenAndUserFromRequest(email);
        } catch (Exception e) {
            response = new CommonResponse("There was en error processing your request. Please try again later.");
            status = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(response, status);
    }

    private CommonResponse getTokenAndUserFromRequest(String email) {
        final UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        final String jwt = jwtTokenUtil.generateToken(userDetails);

        UserDTO userDTO = new UserDTO(userService.getUserByEmail(email));

        return new CommonResponse(jwt, userDTO);
    }
}
