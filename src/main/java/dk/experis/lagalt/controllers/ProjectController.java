package dk.experis.lagalt.controllers;

import dk.experis.lagalt.models.DTOs.CommonResponse;
import dk.experis.lagalt.models.DTOs.NewProjectDTO;
import dk.experis.lagalt.models.DTOs.ProjectDTO;
import dk.experis.lagalt.models.Project;
import dk.experis.lagalt.services.ProjectService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Project Controller", description = "CRUDS for projects")
@RestController
@RequestMapping("/projects")
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    /**
     * Fetches all of the projects from the database.
     *
     * @return a list of projects if successful, else return 404.
     */
    @Operation(summary = "Get all projects", responses = {@ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "204", description = "There are no projects.")})
    @CrossOrigin
    @GetMapping
    public ResponseEntity<CommonResponse> getAllProjects() {
        HttpStatus status = HttpStatus.OK;
        List<Project> projects = projectService.getAllProjects();
        if (projects.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }

        CommonResponse response = new CommonResponse(projects);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Fetches one project from the database based on its id.
     *
     * @param id the ID to check for a specific project.
     * @return the specific project if successful, else return 404.
     */
    @Operation(summary = "Get project", description = "Get a project with the provided id.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "A project with the provided id does not exist.")
    @Parameter(name = "id", description = "Id of the project to get.")
    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<CommonResponse> getProject(@PathVariable long id) {
        HttpStatus status = HttpStatus.NO_CONTENT;
        Project returnProject = projectService.getProjectById(id);
        if (returnProject != null) {
            status = HttpStatus.OK;
        }
        CommonResponse response = new CommonResponse(returnProject);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Gets all projects a user is currently a part of.
     *
     * @param userId the ID to check for projects.
     * @return the list of projects for the given user if successful, else return 404.
     */
    @Operation(summary = "Get active projects for user.", description = "Gets all projects a given user is currently " +
            "active in.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "The user is not active in any projects.")
    @Parameter(name = "userId", description = "Id of the user to get the active projects from.")
    @CrossOrigin
    @GetMapping("/user/{userId}/active")
    public ResponseEntity<CommonResponse> getActiveProjectsForUser(@PathVariable long userId) {
        HttpStatus status = HttpStatus.OK;
        List<Project> activeProjects = projectService.getActiveProjectsFromUser(userId);
        if (activeProjects == null) {
            status = HttpStatus.NO_CONTENT;
        }
        CommonResponse response = new CommonResponse(activeProjects);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Gets all projects that has a specified tag.
     *
     * @param tagId the ID to check for projects.
     * @return the list of projects in the given tag if successful, else return 404.
     */
    @Operation(summary = "Get projects from tag.", description = "Gets all projects with a given tag..")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "The are no projects with this tag.")
    @Parameter(name = "tagId", description = "Id of the tag to get the projects from.")
    @CrossOrigin
    @GetMapping("/tag/{tagId}")
    public ResponseEntity<CommonResponse> getProjectsForTag(@PathVariable long tagId) {
        HttpStatus status = HttpStatus.OK;
        List<Project> activeProjects = projectService.getProjectsFromTagId(tagId);
        if (activeProjects == null) {
            status = HttpStatus.NO_CONTENT;
        }
        CommonResponse response = new CommonResponse(activeProjects);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Gets all projects within an industry.
     *
     * @param industryId the ID to check for projects.
     * @return the list of projects in the given industry if successful, else return 404.
     */
    @Operation(summary = "Get projects in industry.", description = "Gets all projects within a given industry.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "There are no projects in the given industry.")
    @Parameter(name = "industryId", description = "Id of the industry to get the active projects from.")
    @CrossOrigin
    @GetMapping("/industry/{industryId}")
    public ResponseEntity<CommonResponse> getProjectsInIndustry(@PathVariable long industryId) {
        HttpStatus status = HttpStatus.OK;
        List<Project> projectsInIndustry = projectService.getProjectsInIndustry(industryId);
        if (projectsInIndustry.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }
        CommonResponse response = new CommonResponse(projectsInIndustry);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Creates a new project in the Database.
     *
     * @param newProject object to be added.
     * @return 201 if successful, else return 500.
     */
    @Operation(summary = "Create a new project.")
    @ApiResponse(responseCode = "201", description = "Project was created.")
    @ApiResponse(responseCode = "400", description = "Bad request.")
    @Parameter(name = "Project  object", description = "The project to create.")
    @CrossOrigin
    @PostMapping()
    public ResponseEntity<CommonResponse> createProject(@RequestBody NewProjectDTO newProject) {
        HttpStatus status = HttpStatus.OK;
        CommonResponse response;
        try {
            projectService.createProject(newProject);
            response = new CommonResponse(status);
        } catch (Exception e) {
            status = HttpStatus.BAD_REQUEST;
            response = new CommonResponse(e.getMessage());
        }

        return new ResponseEntity<>(response, status);
    }

    /**
     * Updates an existing project in the database
     *
     * @param projectId      to search for a specific project.
     * @param updatedProject the project object to update
     * @return 200 if successful, else return 404.
     */
    @Hidden
    @Operation(summary = "Update project")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "404", description = "Project with the specified id does not exist.")
    @Parameter(name = "id", description = "Id of the project to update.")
    @Parameter(name = "project", description = "The updated project.")
    @PatchMapping("/{projectId}")
    public ResponseEntity<HttpStatus> updateProject(@PathVariable long projectId,
                                                    @RequestBody ProjectDTO updatedProject) {
        if (projectService.projectExists(projectId)) {
            Project existingProject = projectService.getProjectById(projectId);

            if (updatedProject.name != null) {
                existingProject.setName(updatedProject.name);
            }

            if (updatedProject.description != null) {
                existingProject.setDescription(updatedProject.description);
            }

            if (updatedProject.progress != null) {
                existingProject.setProgress(updatedProject.progress);
            }

            projectService.updateProject(existingProject);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

    /**
     * Soft deletes a project from the database.
     *
     * @param projectId to find the specific project
     * @return 200 if successful, else return 404.
     */
    @Hidden
    @Operation(summary = "Delete project.")
    @ApiResponse(responseCode = "200", description = "Project was deleted.")
    @ApiResponse(responseCode = "404", description = "The requested project does not exist.")
    @Parameter(name = "id", description = "Id of the project to delete.")
    @DeleteMapping("/{projectId}")
    public ResponseEntity<HttpStatus> deleteProject(@PathVariable long projectId) {
        if (projectService.deleteProject(projectId) != null) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.notFound().build();
    }
}
