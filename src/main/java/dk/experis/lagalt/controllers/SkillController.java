package dk.experis.lagalt.controllers;

import dk.experis.lagalt.models.DTOs.CommonResponse;
import dk.experis.lagalt.models.DTOs.SkillDTO;
import dk.experis.lagalt.models.Skill;
import dk.experis.lagalt.services.SkillService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Tag(name = "Skill Controller", description = "Get skill information")
@RestController
@RequestMapping("/skills")
public class SkillController {

    @Autowired
    private SkillService skillService;

    /**
     * Fetches all skills from the database.
     */
    @Operation(summary = "Get all skills")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "There are no skills.")
    @CrossOrigin
    @GetMapping
    public ResponseEntity<CommonResponse> getAllSkills() {
        HttpStatus status = HttpStatus.OK;
        List<Skill> skills = skillService.getAllSkills();
        if (skills.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }
        List<SkillDTO> skillDTOs =
                skills.stream().map(skill -> new SkillDTO(skill.getId(), skill.getSkillName())).collect(Collectors.toList());

        CommonResponse response = new CommonResponse(skillDTOs);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Get a single skill
     * @param id
     * @return
     */
    @Hidden
    @GetMapping("/{id}")
    public ResponseEntity<CommonResponse> getSkill(@PathVariable long id) {
        HttpStatus status = HttpStatus.NO_CONTENT;
        Skill returnSkill = skillService.getSkillById(id);
        if (returnSkill != null) {
            status = HttpStatus.OK;
        }

        CommonResponse response = new CommonResponse(returnSkill);
        return new ResponseEntity<>(response, status);
    }
}
