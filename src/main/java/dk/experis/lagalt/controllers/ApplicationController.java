package dk.experis.lagalt.controllers;

import dk.experis.lagalt.models.Application;
import dk.experis.lagalt.models.DTOs.ApplicationDTO;
import dk.experis.lagalt.models.DTOs.CommonResponse;
import dk.experis.lagalt.services.ApplicationService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@Tag(name = "Application Controller", description = "CRUDS for project applications")
@RestController
@RequestMapping("/applications")
public class ApplicationController {

    @Autowired
    private ApplicationService applicationService;

    /**
     * Fetches all the applications from the database.
     *
     * @return list of applications.
     */
    @Operation(summary = "Get all application")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "There are no applications.")
    @CrossOrigin
    @GetMapping
    public ResponseEntity<CommonResponse> getAllApplications() {
        HttpStatus status = HttpStatus.OK;
        List<Application> applications = applicationService.getAllApplications();
        if(applications.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }

        CommonResponse response = new CommonResponse(applications);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Fetches a specific application from the database by its id.
     *
     * @param id to find the specific application with.
     * @return specific application object.
     */
    @Operation(summary = "Get an application with the provided id.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "An application with the provided id does not exist.")
    @Parameter(name = "id", description = "Id of the application to get.")
    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<CommonResponse> getApplicationById(@PathVariable long id) {
        HttpStatus status = HttpStatus.NO_CONTENT;
        Application returnApplication = applicationService.getApplicationById(id);
        if (returnApplication != null) {
            status = HttpStatus.OK;
        }

        CommonResponse response = new CommonResponse(returnApplication);
        return new ResponseEntity<>(response, status);

    }

    /**
     * Creates a new application in the database.
     *
     * @param application to be added to the database.
     * @return 201 if successful, else return 500.
     */
    @Operation(summary = "Create a new application.")
    @ApiResponse(responseCode = "201", description = "Application was created.")
    @ApiResponse(responseCode = "400", description = "Bad request.")
    @Parameter(name = "Application  object", description = "The application to create.")
    @PostMapping()
    public ResponseEntity<HttpStatus> createApplication(@RequestBody Application application) {
        Application applicationToCreate = applicationService.createApplication(application);
        if(applicationToCreate != null) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }

        return ResponseEntity.badRequest().build();
    }

    /**
     * Updates an existing application in the database.
     *
     * @param id used for finding the specific application to update
     * @param application object to be updated
     * @return 200 if successful, else return 404.
     */
    @Hidden
    @Operation(summary = "Update application")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "400", description = "Bad request.")
    @Parameter(name = "id", description = "Id of the application to update.")
    @Parameter(name = "application", description = "The updated application.")
    @PutMapping("/{id}")
    public ResponseEntity<HttpStatus> updateApplication(@PathVariable long id, @RequestBody ApplicationDTO application) {
        if(applicationService.applicationExists(id)) {
            Application existingApplication = applicationService.getApplicationById(id);
            if(application.application != null) {
                existingApplication.setApplication(application.application);
            }
            if(application.applicationApproved != null) {
                existingApplication.toggleApproved();
            }
            if(application.applicationResolved != null) {
                existingApplication.toggleResolved();
            }
            
            applicationService.updateApplication(existingApplication);
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    /**
     * Fetches all the applications on a specific project.
     *
     * @param id the specific project Id, to find applications on
     * @return 200 and the list of applications if successful, else return 404.
     */
    @Operation(summary = "Get applications by project.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "The provided project has no applications.")
    @Parameter(name = "id", description = "Id of the project to get the applications for.")
    @CrossOrigin
    @GetMapping("/project/{id}")
    public ResponseEntity<CommonResponse> getApplicationsByProject(@PathVariable long id) {
        HttpStatus status = HttpStatus.OK;
        List<ApplicationDTO> applications = applicationService.getAllByProjectId(id);
        if(applications.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }

        CommonResponse response = new CommonResponse(applications);
        return new ResponseEntity<>(response, status);
    }
}
