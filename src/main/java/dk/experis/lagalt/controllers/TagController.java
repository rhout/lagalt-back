package dk.experis.lagalt.controllers;

import dk.experis.lagalt.models.DTOs.CommonResponse;
import dk.experis.lagalt.models.Tag;
import dk.experis.lagalt.services.TagService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@io.swagger.v3.oas.annotations.tags.Tag(name = "Tag Controller", description = "Get tag information")
@RestController
@RequestMapping("/tags")
public class TagController {

    @Autowired
    private TagService tagService;

    /**
     * Fetches all tags from the database.
     */
    @Hidden
    @Operation(summary = "Get all tags.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "There are no tags.")
    @CrossOrigin
    @GetMapping
    public ResponseEntity<CommonResponse> getAllTags() {
        HttpStatus status;
        List<Tag> tags = tagService.getAllTags();
        if (tags.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.OK;
        }
        CommonResponse response = new CommonResponse(tags);
        return new ResponseEntity<>(response, status);
    }

    /**
     * Gets a single tag based on its id.
     *
     * @param id
     * @return
     */
    @Hidden
    @GetMapping("/{id}")
    public ResponseEntity<Tag> getTag(@PathVariable long id) {
        HttpStatus status = HttpStatus.NO_CONTENT;
        Tag returnTag = tagService.getTagId(id);
        if (returnTag != null) {
            status = HttpStatus.OK;
            return new ResponseEntity<>(returnTag, status);
        }

        return new ResponseEntity<>(null, status);
    }

    /**
     * Fetches the five most popular tags based on number of uses.
     */
    @Operation(summary = "Get popular tags", description = "Get the ten most popular tags based on " +
            "how many projects have that tag.")
    @ApiResponse(responseCode = "200", description = "OK")
    @ApiResponse(responseCode = "204", description = "There are no tags.")
    @CrossOrigin
    @GetMapping("/popular")
    public ResponseEntity<CommonResponse> getPopularTags() {
        HttpStatus status = HttpStatus.OK;
        List<Tag> tags = tagService.getAllTags();
        if (tags.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
        }

        tags.sort(Comparator.comparingInt(tag -> ((Tag) tag).getProjects().size()).reversed());

        if (tags.size() >= 10) {
            tags = tags.subList(0, 10);
        }

        CommonResponse response = new CommonResponse(tags);
        return new ResponseEntity<>(response, status);
    }
}