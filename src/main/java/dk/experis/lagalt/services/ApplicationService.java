package dk.experis.lagalt.services;

import dk.experis.lagalt.controllers.ApplicationController;
import dk.experis.lagalt.models.Application;
import dk.experis.lagalt.models.DTOs.ApplicationDTO;
import dk.experis.lagalt.repositories.ApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class ApplicationService {

    @Autowired
    private ApplicationRepository applicationRepository;

    /**
     * Fetches all the applications from the database.
     *
     * @return the list of applications.
     */
    public List<Application> getAllApplications() { return applicationRepository.findAll(); }

    /**
     * Fetches a specific application from the database.
     *
     * @param id to search for the specific application.
     * @return the specific application object.
     */
    public Application getApplicationById(long id) {
        Application application = null;
        if(applicationRepository.existsById(id)) {
            application = applicationRepository.findById(id).get();
        }

        return application;
    }

    /**
     * Creates an application in the database.
     *
     * @param application object to be added.
     * @return the save method from the repository, adding it to the database.
     */
    public Application createApplication(Application application) {
        return applicationRepository.save(application);
    }

    /**
     * Updates an existing application in the database.
     *
     * @param application object with the updated information.
     * @return updated application object.
     */
    public Application updateApplication(Application application) {
        Application applicationToUpdate = null;
        if(applicationRepository.existsById(application.getId())) {
            applicationToUpdate = applicationRepository.save(application);
        }
        return applicationToUpdate;
    }

    /**
     * Checks if an application exists in the database.
     *
     * @param applicationID the specific ID to search for an application.
     * @return true if it exists, else return false.
     */
    public boolean applicationExists(long applicationID) { return applicationRepository.existsById(applicationID); }

    public List<ApplicationDTO> getAllByProjectId(long id) {
        return convertModelToDTO(applicationRepository.getAllByUserProjectProject_Id(id));
    }

    /**
     * Convert applications classes to DTOs.
     *
     * @param modelList of applications.
     * @return the list of applicationDTOs.
     */
    private List<ApplicationDTO> convertModelToDTO(List<Application> modelList) {
        List<ApplicationDTO> dtos = new ArrayList<>();
        for(Application application : modelList) {
            dtos.add(new ApplicationDTO(application.getId(), application.getApplication(), application.isApplicationApproved(), application.isApplicationResolved()));
        }
        return dtos;
    }
}
