package dk.experis.lagalt.services;

import dk.experis.lagalt.models.Skill;
import dk.experis.lagalt.repositories.SkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SkillService {

    @Autowired
    private SkillRepository skillRepository;

    /**
     * Fetches all skills from the database.
     */
    public List<Skill> getAllSkills() {
        return skillRepository.findAll();
    }

    /**
     * Fetches a skill from the database by its id.
     * @param id
     * return The fetched Skill object
     */

    public Skill getSkillById(long id) {
        Skill skill = null;
        if(skillRepository.existsById(id)) {
            skill = skillRepository.findById(id).get();
        }
        return skill;
    }
}
