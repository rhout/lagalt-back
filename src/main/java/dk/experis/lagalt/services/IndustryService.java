package dk.experis.lagalt.services;

import dk.experis.lagalt.models.DTOs.IndustryDTO;
import dk.experis.lagalt.models.Industry;
import dk.experis.lagalt.repositories.IndustryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IndustryService {
    @Autowired
    private IndustryRepository industryRepository;

    /**
     * Fetches all industries from the database.
     */

    public List<Industry> getAllIndustries() {
        return industryRepository.findAll();
    }


    /**
     * Fetches an industry from the database by its id.
     *
     * @param id
     * @return
     */

    public Industry getIndustryById(long id) {
        Industry industry = null;
        if(industryRepository.existsById(id)) {
            industry = industryRepository.findById(id).get();
        }
        return industry;
    }

    /**
     * Gets all the industries that appear in a specific project from the database.
     *
     * @param id
     * @return
     */

    public List<IndustryDTO> getAllByProjectId(long id) {
        return convertModelToDTO(industryRepository.getAllByProjects_Id(id));
    }

    /**
     * Takes a list of Industreis and converts it to a list of IndustryDTOs.
     *
     * @param modelList
     * @return
     */
    private List<IndustryDTO> convertModelToDTO(List<Industry> modelList) {
        List<IndustryDTO> dtos = new ArrayList<>();
        for (Industry industry : modelList) {
            dtos.add(new IndustryDTO(industry.getId(), industry.getName()));
        }
        return dtos;
    }




}
