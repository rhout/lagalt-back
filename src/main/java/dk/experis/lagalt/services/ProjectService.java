package dk.experis.lagalt.services;

import dk.experis.lagalt.models.*;
import dk.experis.lagalt.models.DTOs.NewProjectDTO;
import dk.experis.lagalt.repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private UserProjectService userProjectService;
    @Autowired
    private ProjectSkillService projectSkillService;
    @Autowired
    private TagService tagService;

    /**
     * Fetches all the projects from the database.
     *
     * @return list of all the projects.
     */
    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    /**
     * Get a project from a specific Id.
     *
     * @param id to check for a project
     * @return the specific project.
     */
    public Project getProjectById(long id) {
        Project project = null;
        if (projectRepository.existsById(id)) {
            project = projectRepository.findById(id).get();
        }
        return project;
    }

    /**
     * Gets the active projects for a specified user.
     *
     * @param userId to check for projects.
     * @return the list of active projects for the user.
     */
    public List<Project> getActiveProjectsFromUser(long userId) {
        List<UserProject> userProjects = userProjectService.getAllByActiveUser(userId);
        List<Project> projects = new ArrayList<>();
        for (UserProject userProject : userProjects) {
            Project project = projectRepository.getOne(userProject.getProjectId());
            projects.add(project);
        }
        return projects;
    }

    /**
     * Gets all the projects with a specific tag.
     *
     * @param tagId to check for projects.
     * @return the list of projects.
     */
    public List<Project> getProjectsFromTagId(long tagId) {
        return projectRepository.findAllByTags_Id(tagId);
    }

    /**
     * Gets all the projects within an industry
     *
     * @param industryId to check for projects.
     * @return the list of projects.
     */
    public List<Project> getProjectsInIndustry(long industryId) {
        return projectRepository.getAllByIndustry_Id(industryId);
    }

    /**
     * Creates a new project
     *
     * @param newProject object to be created
     */
    public void createProject(NewProjectDTO newProject) {
        Set<Tag> projectTags = tagService.getTagsFromNewProject(newProject.tags);

        Project projectToCreate = new Project(newProject.name, newProject.description, newProject.projectImage,
                newProject.industry, projectTags);

        projectToCreate = projectRepository.save(projectToCreate);

        projectSkillService.createNeededSkillsFromNewProject(newProject.neededSkills, projectToCreate);
        userProjectService.createUserProjectsForNewProject(newProject.projectOwners, projectToCreate);
    }

    /**
     * Check whether or not a specific project exists in the database.
     *
     * @param projectId to check for the specific project
     * @return true if it exists, false if it doesn't.
     */
    public boolean projectExists(long projectId) {
        return projectRepository.existsById(projectId);
    }

    /**
     * Update an existing project in the database.
     *
     * @param project object to updated
     * @return the newly updated project object.
     */
    public Project updateProject(Project project) {
        Project projectToUpdate = null;

        if (projectRepository.existsById(project.getId())) {
            projectToUpdate = projectRepository.save(project);
        }

        return projectToUpdate;
    }

    /**
     * Soft deleted a project from the database.
     *
     * @param projectId to find the specific project.
     * @return the deleted project.
     */
    public Project deleteProject(long projectId) {
        Project projectToDelete = null;
        if (projectRepository.existsById(projectId)) {
            projectToDelete = projectRepository.findById(projectId).get();
            projectToDelete.setDeleted(true);
            projectRepository.save(projectToDelete);
        }
        return projectToDelete;
    }
}
