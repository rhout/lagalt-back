package dk.experis.lagalt.services;

import dk.experis.lagalt.models.DTOs.SkillDTO;
import dk.experis.lagalt.models.Project;
import dk.experis.lagalt.models.ProjectSkill;
import dk.experis.lagalt.models.Skill;
import dk.experis.lagalt.repositories.ProjectSkillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectSkillService {
    @Autowired
    private ProjectSkillRepository projectSkillRepository;
    @Autowired
    private SkillService skillService;

    /**
     * Creates projectSkills for the newly created project.
     *
     * @param neededSkills    The skills sent from the frontend
     * @param projectToCreate The newly created project.
     */
    public void createNeededSkillsFromNewProject(List<SkillDTO> neededSkills, Project projectToCreate) {
        List<Long> neededSkillsIds = neededSkills.stream().map(SkillDTO::getId).collect(Collectors.toList());
        for (long skillId : neededSkillsIds) {
            Skill skillToAdd = skillService.getSkillById(skillId);
            ProjectSkill projectSkill = new ProjectSkill(skillToAdd, projectToCreate);
            createProjectSkill(projectSkill);
        }
    }

    /**
     * Creates a new project skill in the database.
     * @param projectSkill
     */
    public void createProjectSkill(ProjectSkill projectSkill) {
        projectSkillRepository.save(projectSkill);
    }
}
