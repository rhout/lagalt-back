package dk.experis.lagalt.services;

import dk.experis.lagalt.models.Tag;
import dk.experis.lagalt.repositories.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TagService {
    @Autowired
    private TagRepository tagRepository;

    /**
     * Fetches all tags from the database.
     */
    public List<Tag> getAllTags() {
        return tagRepository.findAll();
    }

    /**
     * Checks if a tag exists in the database
     * @param tagName
     * return
     */
    public boolean tagExistsByName(String tagName) {
        return tagRepository.existsByName(tagName);
    }

    /**
     * Fetches a tag from the database by its id.
     * @param id
     * return The fetched Tag object
     */
    public Tag getTagId(long id) {
        Tag tag = null;
        if(tagRepository.existsById(id)) {
            tag = tagRepository.findById(id).get();
        }
        return tag;
    }

    /**
     * Fetches a tag from the database by its name.
     * @param name Name of the tag
     * return The fetched Tag object
     */
    public Tag getTagByName(String name) {
        return tagRepository.getByName(name);
    }

    /**
     * Creates a list of tags from a list of tag names
     * @param tagNames
     * @return
     */
    public Set<Tag> getTagsFromNewProject(String[] tagNames) {
        Set<Tag> projectTags = new HashSet<>();
        for (String tagName : tagNames) {
            Tag tag;
            if (tagExistsByName(tagName)) {
                tag = getTagByName(tagName);
            } else {
                tag = createTag(new Tag(tagName));
            }
            projectTags.add(tag);
        }
        return projectTags;
    }

    /**
     * Created a new tag
     * @param tag
     * return the created Tag
     */
    public Tag createTag(Tag tag) {
        return tagRepository.save(tag);
    }
}
