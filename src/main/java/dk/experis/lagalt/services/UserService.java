package dk.experis.lagalt.services;

import dk.experis.lagalt.models.DTOs.SkillDTO;
import dk.experis.lagalt.models.Skill;
import dk.experis.lagalt.models.User;
import dk.experis.lagalt.repositories.SkillRepository;
import dk.experis.lagalt.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SkillService skillService;

    /**
     * Checks if a user with the provided id exists in the database.
     *
     * @param userId
     * @return
     */
    public boolean userExists(long userId) {
        return userRepository.existsById(userId);
    }

    /**
     * Checks if a user with the provided email exists in the database.
     *
     * @param email
     * @return
     */
    public boolean userExistsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    /**
     * Fetches a user from the database based on the id.
     */
    public User getUserById(long id) {
        try {
            return userRepository.findById(id).get();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    /**
     * Fetches a user based on the email.
     */
    public User getUserByEmail(String email) {
        try {
            return userRepository.findByEmail(email);
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    /**
     * Updates the user in the database.
     *
     * @param user
     */
    public void updateUser(User user) {
        userRepository.save(user);
    }

    /**
     * Creates a user in the database with credentials.
     *
     * @param user
     */
    public void createUser(User user) {
        userRepository.save(user);
    }

    /**
     * Adds a skill to a users current skills based on a list of skills that should be in the user.
     *
     * @param user   The user to have the skill added to
     * @param skills the skills that the user should have
     */
    public void addSkillToUser(User user, List<SkillDTO> skills) {
        List<Long> updatedSkillIds = skills.stream().map(SkillDTO::getId).collect(Collectors.toList());
        List<Long> existingSkillIds = user.getSkills().stream().map(Skill::getId).collect(Collectors.toList());
        for (long skillId : updatedSkillIds) {
            if (!existingSkillIds.contains(skillId)) {
                Skill skillToAdd = skillService.getSkillById(skillId);
                user.addSkill(skillToAdd);
            }
        }
    }

    /**
     * Deletes a skill from a user based on a list of skills that the user should have.
     *
     * @param user   The user to have the skill added to
     * @param skills the skills that the user should have
     */
    public void deleteSkillFromUser(User user, List<SkillDTO> skills) {
        List<Long> skillIds = skills.stream().map(SkillDTO::getId).collect(Collectors.toList());
        for (Skill skill : user.getSkills()) {
            long existingSkillId = skill.getId();
            if (!skillIds.contains(existingSkillId)) {
                Skill skillToRemove = skillService.getSkillById(existingSkillId);
                user.deleteSkill(skillToRemove);
                break;
            }
        }
    }
}
