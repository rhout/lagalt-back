package dk.experis.lagalt.services;

import dk.experis.lagalt.models.*;
import dk.experis.lagalt.repositories.UserProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserProjectService {
    @Autowired
    private UserProjectRepository userProjectRepository;

    /**
     * Gets all the active user projects for a specified user
     * @param userId
     * @return
     */
    public List<UserProject> getAllByActiveUser(long userId) {
        return userProjectRepository.getAllByUser_IdAndIsActiveIsTrue(userId);
    }

    /**
     * Creates userProjects for the newly created project.
     *
     * @param projectOwners    The skills sent from the frontend
     * @param projectToCreate The newly created project.
     */
    public void createUserProjectsForNewProject(List<User> projectOwners, Project projectToCreate) {
        for (User user : projectOwners) {
            UserProject userProject = new UserProject(user, projectToCreate);
            createUserProject(userProject);
        }
    }

    /**
     * Creates a new userProject in the database
     */
    public void createUserProject(UserProject userProject) {
        userProjectRepository.save(userProject);
    }
}
