package dk.experis.lagalt.repositories;

import dk.experis.lagalt.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    Tag getByName(String name);
    boolean existsByName(String name);
}
