package dk.experis.lagalt.repositories;

import dk.experis.lagalt.models.UserProject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserProjectRepository extends JpaRepository<UserProject, Long> {
    List<UserProject> getAllByUser_IdAndIsActiveIsTrue(long userId);
}
