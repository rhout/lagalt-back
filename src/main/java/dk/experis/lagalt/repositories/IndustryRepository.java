package dk.experis.lagalt.repositories;

import dk.experis.lagalt.models.Industry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IndustryRepository extends JpaRepository<Industry, Long> {
    List<Industry> getAllByProjects_Id(long id);
}
