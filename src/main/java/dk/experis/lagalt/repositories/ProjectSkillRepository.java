package dk.experis.lagalt.repositories;

import dk.experis.lagalt.models.ProjectSkill;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectSkillRepository extends JpaRepository<ProjectSkill, Long> {
}
