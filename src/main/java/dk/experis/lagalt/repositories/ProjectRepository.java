package dk.experis.lagalt.repositories;

import dk.experis.lagalt.models.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    List<Project> findAllByTags_Id(long tagId);
    List<Project> getAllByIndustry_Id(long industryId);
}
