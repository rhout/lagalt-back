package dk.experis.lagalt.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import dk.experis.lagalt.models.DTOs.SkillDTO;
import io.swagger.v3.oas.annotations.Hidden;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Hidden
@Entity
@Table(name = "projects")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private String projectImage;
    private Progress progress = Progress.IN_PROGRESS;
    private Date dateCreated = new Date();
    private boolean deleted = false;

    @OneToMany(mappedBy = "project")
    List<UserProject> userProjects;

    @JsonGetter("userProjects")
    public List<String> userProjects() {
        if (userProjects != null) {
            return userProjects.stream()
                    .map(userProject -> "/api/v1/user_project/" + userProject.getId()).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    @JsonGetter("projectActiveUsers")
    public List<User> projectActiveUsers() {
        if (userProjects != null) {
            return userProjects.stream()
                    .filter(UserProject::isActive)
                    .map(UserProject::getUser).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    @JsonGetter("projectOwners")
    public List<User> projectOwners() {
        if (userProjects != null) {
            return userProjects.stream()
                    .filter(UserProject::isOwner)
                    .map(UserProject::getUser).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    @OneToMany(mappedBy = "project")
    List<ProjectSkill> projectSkills;


    @JsonGetter("projectSkills")
    public List<String> projectSkills() {
        if (projectSkills != null) {
            return projectSkills.stream()
                    .map(projectSkill -> "/api/v1/skills/" + projectSkill.getId()).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    @JsonGetter("neededSkills")
    public List<SkillDTO> neededProjectSkills() {
        if (projectSkills != null) {
            return projectSkills.stream()
                    .filter(ProjectSkill::isNeeded)
                    .map(skill -> new SkillDTO(skill.getId(), skill.getSkillName())).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    @ManyToOne
    Industry industry;

    @JsonGetter("industry")
    public Industry industry() {
        if (industry != null) {
            return industry;
        } else {
            return null;
        }
    }

    @ManyToMany
    @JoinTable(
            name = "project_tag",
            joinColumns = {@JoinColumn(name = "project_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}
    )
    Set<Tag> tags;

    @JsonGetter("tags")
    public List<String> tags() {
        if (tags != null) {
            return tags.stream()
                    .map(Tag::getName).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public Project() {
    }

    public Project(String name, String description, String projectImage, Industry industry, Set<Tag> tags) {
        this.name = name;
        this.description = description;
        this.projectImage = projectImage;
        this.industry = industry;
        this.tags = tags;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getProjectImage() {
        return projectImage;
    }

    public void setProjectImage(String projectImage) {
        this.projectImage = projectImage;
    }

    public Progress getProgress() {
        return progress;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProgress(Progress progress) {
        this.progress = progress;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
