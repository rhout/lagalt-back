package dk.experis.lagalt.models;


import com.fasterxml.jackson.annotation.JsonGetter;
import dk.experis.lagalt.models.DTOs.SkillDTO;
import io.swagger.v3.oas.annotations.Hidden;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

@Hidden
@Entity
@Table(name = "users")
@Where(clause = "deleted = false")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String password;
    private String description;
    private String portfolio;
    private String email;
    private String image;
    private Date dateCreated = new Date();
    private boolean hidden = false;
    private boolean deleted = false;

    @OneToMany(mappedBy = "user")
    List<UserProject> userProjects;

    @JsonGetter("userProjects")
    public List<String> userProjects() {
        if (userProjects != null) {
            return userProjects.stream()
                    .map(userProject -> "/api/v1/user_project/" + userProject.getId()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @ManyToMany
    @JoinTable(
            name = "user_skill",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "skill_id")}
    )
    Set<Skill> skills;

    @JsonGetter("skills")
    public List<SkillDTO> skills() {
        if (skills != null) {
            return skills.stream()
                    .map(skill -> new SkillDTO(skill.getId(), skill.getSkill())).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(String name, String description, String email) {
        this.name = name;
        this.description = description;
        this.email = email;
    }

    public User(String name, String password, String description, String portfolio, String email, String image) {
        this.name = name;
        this.password = password;
        this.description = description;
        this.portfolio = portfolio;
        this.email = email;
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    @NaturalId
    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isHidden() {
        return hidden;
    }

    public String getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(String portfolio) {
        this.portfolio = portfolio;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void addSkill(Skill skill) {
        long id = skill.getId();
        if (hasNotSkill(id)) {
            this.skills.add(skill);
        }
    }

    private boolean hasNotSkill(long newSkillId) {
        return !(this.skills.stream().filter(s -> s.getId() == newSkillId).collect(Collectors.toSet()).size() > 0);
    }

    public void deleteSkill(Skill skill) {
        this.skills.remove(skill);
    }

    public void toggleHidden() {
        this.hidden = !hidden;
    }
}
