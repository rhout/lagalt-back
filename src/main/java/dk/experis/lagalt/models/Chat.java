package dk.experis.lagalt.models;

import io.swagger.v3.oas.annotations.Hidden;

import javax.persistence.*;
import java.util.Date;

@Hidden
@Entity
@Table(name = "chats")
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String chatMessage;
    private Date dateCreated;
    private boolean deleted = false;

    @ManyToOne
    private UserProject userProject;
}
