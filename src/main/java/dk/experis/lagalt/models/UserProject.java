package dk.experis.lagalt.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import io.swagger.v3.oas.annotations.Hidden;

import javax.persistence.*;
import java.util.List;

@Hidden
@Entity
public class UserProject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private boolean isOwner;
    private boolean isActive;

    public UserProject() {
    }

    public UserProject(User user, Project project) {
        this.isOwner = true;
        this.isActive = true;
        this.user = user;
        this.project = project;
    }

    @OneToMany(mappedBy = "userProject")
    List<Chat> chats;

    @OneToMany(mappedBy = "userProject")
    List<Message> messages;

    @OneToMany(mappedBy = "userProject")
    List<Application> applications;

    @OneToMany(mappedBy = "userProject")
    List<Resource> resources;

    @ManyToOne
    private User user;

    @JsonGetter("user")
    public String industry() {
        if (user != null) {
            return "/api/v1/users/" + user.getId();
        } else {
            return null;
        }
    }

    public UserProject(long id, boolean isOwner, boolean isActive, List<Chat> chats, List<Message> messages, List<Application> applications, List<Resource> resources, User user, Project project) {
        this.id = id;
        this.isOwner = isOwner;
        this.isActive = isActive;
        this.chats = chats;
        this.messages = messages;
        this.applications = applications;
        this.resources = resources;
        this.user = user;
        this.project = project;
    }

    @ManyToOne
    private Project project;

    @JsonGetter("project")
    public String project() {
        if (project != null) {
            return "/api/v1/projects/" + project.getId();
        } else {
            return null;
        }
    }

    public long getId() {
        return id;
    }

    public long getProjectId() {
        return project.getId();
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean isOwner() { return isOwner; }

    public User getUser() { return user; }

    public List<Resource> getResources() {
        return resources;
    }
}
