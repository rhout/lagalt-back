package dk.experis.lagalt.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import io.swagger.v3.oas.annotations.Hidden;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Hidden
@Entity
@Table(name = "tags")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NaturalId
    private String name;

    @ManyToMany(mappedBy = "tags")
    Set<Project> projects;

    @JsonGetter("projects")
    public Set<String> projects() {
        if (projects != null) {
            return projects.stream()
                    .map(project -> "/api/v1/project/" + project.getId()).collect(Collectors.toSet());
        } else {
            return new HashSet<>();
        }
    }

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public Tag(long id, String name, Set<Project> projects) {
        this.id = id;
        this.name = name;
        this.projects = projects;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Project> getProjects() {
        return projects;
    }
}
