package dk.experis.lagalt.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "applications")
public class Application {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String application;
    private Boolean applicationResolved;
    private Boolean applicationApproved;
    private Date dateCreated;
    private boolean deleted = false;

    @ManyToOne(cascade = CascadeType.ALL)
    private UserProject userProject;

    public Application(long id, String application, boolean applicationResolved, boolean applicationApproved, Date dateCreated, boolean deleted) {
        this.id = id;
        this.application = application;
        this.applicationResolved = applicationResolved;
        this.applicationApproved = applicationApproved;
        this.dateCreated = dateCreated;
        this.deleted = deleted;
    }

    @JsonGetter
    public String getUserProject() {
        if(userProject != null) {
            return "api/v1/user_project/" + userProject.getId();
        } else {
            return null;
        }
    }

    public Application() {

    }

    public long getId() {
        return id;
    }

    public String getApplication() {
        return application;
    }

    public boolean isApplicationResolved() {
        return applicationResolved;
    }

    public boolean isApplicationApproved() {
        return applicationApproved;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public void setApplicationResolved(boolean applicationResolved) {
        this.applicationResolved = applicationResolved;
    }

    public void setApplicationApproved(boolean applicationApproved) {
        this.applicationApproved = applicationApproved;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void toggleApproved() {
        this.applicationApproved = !applicationApproved;
    }

    public void toggleResolved() {
        this.applicationResolved = !applicationResolved;
    }

}
