package dk.experis.lagalt.models;

import io.swagger.v3.oas.annotations.Hidden;

import javax.persistence.*;
import java.util.Date;

@Hidden
@Entity
@Table(name = "messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String title;
    private String message;
    private Date dateCreated;
    private boolean deleted = false;

    @ManyToOne
    private UserProject userProject;
}
