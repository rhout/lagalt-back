package dk.experis.lagalt.models.DTOs;

import dk.experis.lagalt.models.Progress;

public class ProjectDTO {
    public long id;
    public String name;
    public String description;
    public Progress progress;

}
