package dk.experis.lagalt.models.DTOs;

import dk.experis.lagalt.models.Industry;
import dk.experis.lagalt.models.User;

import java.util.List;

public class NewProjectDTO {
    public String name;
    public String description;
    public String projectImage;
    public Industry industry;
    public List<User> projectOwners;
    public List<SkillDTO> neededSkills;
    public String[] tags;
}
