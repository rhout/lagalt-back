package dk.experis.lagalt.models.DTOs;

public class NewUserDTO {
    public String email;
    public String password;
    public String name;
    public String description;
    public String portfolio;
    public String image;
}
