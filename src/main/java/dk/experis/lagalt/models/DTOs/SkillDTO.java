package dk.experis.lagalt.models.DTOs;

import io.swagger.v3.oas.annotations.Hidden;

@Hidden

public class SkillDTO {
    private long id;
    private String skill;

    public SkillDTO(long id, String skill) {
        this.id = id;
        this.skill = skill;
    }

    public long getId() {
        return id;
    }

    public String getSkill() {
        return skill;
    }
}
