package dk.experis.lagalt.models.DTOs;

import io.swagger.v3.oas.annotations.Hidden;

@Hidden
public class IndustryDTO {
    public long id;
    public String name;

    public IndustryDTO() {
    }

    public IndustryDTO(long id, String name) {
        this.id = id;
        this.name = name;
    }
}
