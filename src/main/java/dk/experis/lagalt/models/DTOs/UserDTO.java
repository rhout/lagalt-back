package dk.experis.lagalt.models.DTOs;

import dk.experis.lagalt.models.User;

import java.util.List;

public class UserDTO {
    public long id;
    public String name;
    public String email;
    public String description;
    public String portfolio;
    public String image;
    public Boolean hidden;
    public List<SkillDTO> skills;

    public UserDTO() {
    }

    public UserDTO(User user) {
        id = user.getId();
        email = user.getEmail();
        description = user.getDescription();
        portfolio = user.getPortfolio();
        image = user.getImage();
        hidden = user.isHidden();
        skills = user.skills();
    }
}
