package dk.experis.lagalt.models.DTOs;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Common response for all controllers containing either the requested data, an authentication token, or an error message.")
public class CommonResponse {
    public String token;
    public Object data;
    public String error;

    public CommonResponse(Object data) {
        this.data = data;
    }

    public CommonResponse(String error) {
        this.error = error;
    }

    public CommonResponse(String token, Object data) {
        this.data = data;
        this.token = token;
    }
}
