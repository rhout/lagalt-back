package dk.experis.lagalt.models.DTOs;

public class ApplicationDTO {
    public long Id;
    public String application;
    public Boolean applicationResolved;
    public Boolean applicationApproved;

    public ApplicationDTO() {
    }

    public ApplicationDTO(long id, String application, Boolean applicationResolved, Boolean applicationApproved) {
        Id = id;
        this.application = application;
        this.applicationResolved = applicationResolved;
        this.applicationApproved = applicationApproved;
    }
}


