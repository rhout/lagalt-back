package dk.experis.lagalt.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import io.swagger.v3.oas.annotations.Hidden;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Hidden
@Entity
@Table(name = "skills")
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private SkillEnum skill;

    @OneToMany(mappedBy = "skill")
    List<ProjectSkill> projectSkills;

    public String getSkill() {
        return skill.toString();
    }

    public String getSkillName() {
        return skill.toString();
    }

    public List<ProjectSkill> getProjectSkills() {
        return projectSkills;
    }

    public List<UserProject> getUserProjects() {
        return userProjects;
    }

    public Set<User> getUsers() {
        return users;
    }

    public Skill(long id, SkillEnum skill, List<ProjectSkill> projectSkills, List<UserProject> userProjects, Set<User> users) {
        this.id = id;
        this.skill = skill;
        this.projectSkills = projectSkills;
        this.userProjects = userProjects;
        this.users = users;
    }

    public Skill() {

    }


    @JsonGetter("projectSkills")
    public List<String> projectSkills() {
        if (projectSkills != null) {
            return projectSkills.stream()
                    .map(projectSkill -> "/api/v1/project_skill/" + projectSkill.getId()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @OneToMany(mappedBy = "project")
    List<UserProject> userProjects;

    @JsonGetter("userProjects")
    public List<String> userProjects() {
        if (userProjects != null) {
            return userProjects.stream()
                    .map(userProject -> "/api/v1/user_project/" + userProject.getId()).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @ManyToMany(mappedBy = "skills")
    Set<User> users;

    @JsonGetter("users")
    public Set<String> users() {
        if (users != null) {
            return users.stream()
                    .map(user -> "/api/v1/users/" + user.getId()).collect(Collectors.toSet());
        } else {
            return new HashSet<>();
        }
    }

    public long getId() {
        return id;
    }
}
