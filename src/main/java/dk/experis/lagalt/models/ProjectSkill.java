package dk.experis.lagalt.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import io.swagger.v3.oas.annotations.Hidden;

import javax.persistence.*;

@Hidden
@Entity
@Table(name = "project_skill")
public class ProjectSkill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private boolean isNeeded = true;

    public ProjectSkill() {
    }

    public ProjectSkill(Skill skill, Project project) {
        this.skill = skill;
        this.project = project;
    }

    @ManyToOne
    private Skill skill;

    @JsonGetter("skill")
    public String skill() {
        if (skill != null) {
            return "/api/v1/skills/" + skill.getId();
        } else {
            return null;
        }
    }

    @ManyToOne
    private Project project;

    @JsonGetter("project")
    public String project() {
        if (project != null) {
            return "/api/v1/projects/" + project.getId();
        } else {
            return null;
        }
    }

    public long getId() {
        return id;
    }

    public String getSkillName() {
        return skill.getSkill();
    }

    public Skill getSkill() {
        return skill;
    }

    public boolean isNeeded() {
        return isNeeded;
    }
}
