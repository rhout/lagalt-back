package dk.experis.lagalt.models;

import io.swagger.v3.oas.annotations.Hidden;

import javax.persistence.*;
import java.util.Date;

@Hidden
@Entity
@Table(name = "resources")
public class Resource {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String resource;
    private ResourceEnum resourceType;
    private Date dateCreated;
    private boolean deleted = false;

    @ManyToOne
    private UserProject userProject;

    public long getId() {
        return id;
    }

    public String getResource() {
        return resource;
    }
}
