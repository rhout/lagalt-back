package dk.experis.lagalt.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum SkillEnum {
    @JsonProperty("Web development")
    WEB_DEVELOPMENT("Web development"),
    @JsonProperty("Game development")
    GAME_DEVELOPMENT("Game development"),
    @JsonProperty("Painting")
    PAINTING("Painting"),
    @JsonProperty("Guitar")
    GUITAR("Guitar"),
    @JsonProperty("Film making")
    FILMING("Film making"),
    @JsonProperty("Acting")
    ACTING("Acting");

    private String string;

    SkillEnum(String name) {
        string = name;
    }

    @Override
    public String toString() {
        return string;
    }

}
