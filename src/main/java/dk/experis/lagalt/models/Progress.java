package dk.experis.lagalt.models;

public enum Progress {
    FOUNDING("Founding"),
    IN_PROGRESS("In progress"),
    STALLED("Stalled"),
    COMPLETED("Completed");

    private String string;

    Progress(String name) {
        string = name;
    }

    @Override
    public String toString() {
        return string;
    }
}
