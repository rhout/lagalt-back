package dk.experis.lagalt.utils;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class UserDetailsMapper {
    public UserDetails toUserDetails(dk.experis.lagalt.models.User user) {
        return User.withUsername(user.getEmail()).password(user.getPassword()).authorities(new ArrayList<>()).build();
    }
}
