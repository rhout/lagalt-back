package dk.experis.lagalt;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Lagalt"))
public class LagaltApplication {

    public static void main(String[] args) {
        SpringApplication.run(LagaltApplication.class, args);
    }

}
